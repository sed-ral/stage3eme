# Programmation avec la carte microbit.

## Déroulement

On dispose d'une [carte microbit]() et du [kit octopus](https://www.elecfreaks.com/learn-en/microbitKit/Tinker_Kit/octopus_bit.html). On programme depuis le site [vittascience](https://fr.vittascience.com/microbit).

* [Prise en main de la carte](https://fr.vittascience.com/learn/tutorial.php?id=25/prise-en-main-de-la-carte-microbit)
* [Coeur clignotant](https://fr.vittascience.com/learn/tutorial.php?id=640/coeur-clignotant)
* [Allumage feu de détresse](https://fr.vittascience.com/learn/tutorial.php?id=133/simulation-de-l-allumage-des-feux-de-detresse)
* [Mesure de la luminosité](https://fr.vittascience.com/learn/tutorial.php?id=28/mesurer-la-luminosite-avec-micro-bit)
* [Ateliers Abracodabra](https://fr.vittascience.com/userDetails?id=28990)
  * Boule magique
  * Comprendre le magnétisme
  * envoyer des messages Morse par onde radio
  * Création d'un mini golf avec un servomoteur
* [Capteur aération](https://fr.vittascience.com/learn/tutorial.php?id=339/guide-d-utilisation-alerte-aeration-micro-bit)
* [Tinker Academy](https://www.elecfreaks.com/learn-en/microbitKit/Tinker_Kit/index.html)
* [Space Invaders](https://www.youtube.com/watch?v=ImPEoy-d_3w) avec le [makecode microbit](https://makecode.microbit.org/S93999-69604-59729-45981)

D'autres sites pour trouver des idées:

* https://microbit.org/fr/projects/
* https://makecode.microbit.org/
* https://scratch.mit.edu/microbit

Le projet [Tete laser](./tete_laser.md) dont le sujet est la conception complete d'une tete avec 2 servo-moteurs est également programmée en microbit

### Idée pour passage au python

La fonction d'affichage sur les led dans l'interface graphique ne permet d'afficher que des images binaires, or les leds peuvent avoir 10 niveau d'éclairage.
* regarder le code python généré par l'interface graphique pour afficher une image binaire, et modifier ce code en jouant sur la chaine de carractère qui l'encode afin d'afficher un dégradé de lumiere
* implémenter le défilement de ce dégradé (droite à gauche ou gauche à droite ) : décaller les valeurs de la chaine de caractères pour chaque image, insérer une temporisation. Chercher la fonction de temporisation dans l'interface graphique et utiliser ce code (explication des boucles, des imports python)
* utiliser des variables pour chaque image (chaque position du dégradé), et les mettre dans une liste python. Utiliser cette liste avec une variable d'index pour refaire l'étape précédente avec un code plus succin (explication des variables et des listes python)
* utiliser les boutons pour contrôler les boutons (en changeant la façon dont évolue la variable d'index) . Cherche la fonctionalité des boutons dans l'interface graphique ainsi que le code généré (explication des tests python)

## Pb. connection sur Ubuntu 22

* A utiliser avec Chromium
* <https://askubuntu.com/questions/1317548/webusb-doesnt-work-connecting-to-microbit>

```
> sudo apt-get install firmware-microbit-micropython
> sudo snap install chromium --channel=candidate/raw-usb 
> sudo snap connect chromium:raw-usb
> sudo bash -c 'echo SUBSYSTEM==\"usb\", ATTR{idVendor}==\"0d28\", MODE=\"0664\", GROUP=\"plugdev\" > /etc/udev/rules.d/50-microbit.rules'
> sudo udevadm control --reload-rules
```

## TODO

* Achat 2 robots [DFRobot Maqueen](https://www.gotronic.fr/art-chassis-micro-maqueen-28705.htm) et 2 cartes Microbit supplémentaires
* Robot Maqueen sur [vitascience](https://fr.vittascience.com/shop/97/robot-maqueen-pour-carte-micro-bit)
* Christine leur fait concevoir une 'pince' pour pousser une balle de ping-pong. Un peu dans cet esprit [Maqueen Foot](http://www.robot24.fr/yes-we-play-soccer-au-college-de-vergt/)
