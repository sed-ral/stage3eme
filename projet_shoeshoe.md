# Projet ShoeShoe

## Introduction

Une action exploratoire nommé [humanlab-Inria](https://project.inria.fr/humanlabinria/fr/) permet de contribuer aux réseaux des associations [Humanlab](https://myhumankit.org/le-humanlab/)

Les [Humanlabs](https://myhumankit.org/le-humanlab/) sont des espaces collaboratifs de fabrication numérique ou de réparation d’objets, ouverts à des personnes présentant un handicap pour leur permettre de s’approprier la technologie pour leur usage propre. On travaille souvent avec celui de Rennes: [MyHumanKit](https://myhumankit.org/) ou celui de Palavas-les-flots: [Humanlab Saint-Pierre](https://www.humanlabsaintpierre.org/).

Tu peux également regarder le [reportage de France2](https://www.francetvinfo.fr/replay-magazine/france-2/13h15/13h15-du-samedi-13-juin-2020_3984397.html) sur Nicolas Huchet le fondateur de MyHumanKit.

Pour te faire une idée, tu peux parcourir la documentation des différents projets sur ce [wiki](https://wikilab.myhumankit.org/).

On contribue à plusieurs projets dont:

* [Exofinger](https://wikilab.myhumankit.org/index.php?title=Projets:Exofinger_:_Thumb)
* [ReadforMe](https://wikilab.myhumankit.org/index.php?title=Projets:Read_For_Me_V3)
* [Magic Control](https://wikilab.myhumankit.org/index.php?title=Projets:Magic_Control_2021)
* Réducteur de sons pour casque
* [Canne-a-son](https://wikilab.myhumankit.org/index.php?title=Projets:Canne_a_son)

On peut citer d'autres actions comme [e-nable](https://e-nable.fr/fr/) qui sont basés sur des fablab orientés à l'aide au handicap avec une philosophie du partage. Il y a également une start-up comme [orthopus](https://orthopus.com/) qui essaye de promouvoir cette philosophie.

Le projet ShoeShoe est un projet de médiation basée sur des techno. open-source [Arduino](https://www.arduino.cc/). Suis les épisodes de la classe de primaire qui a réalisé ce projet avec une chercheuse de Camin [blog shoeshoe](https://savanturiers-projects.cri-paris.org/projects/NhbsL7Uz/blogentries).

Le code est [ici](https://gitlab.inria.fr/humanlab-inria/savanturiers)

Pourrais-tu répondre à ces questions:

* Qu'est ce qu'un FabLAB ? Qu'est ce qu'un wiki ?
* A quoi sert un Humanlab ? Qu'est ce que cela apporte de plus ou de moins vis à vis d'une aide médicale 'classique' ?
* Peux-tu me décrire un projet  que tu as remarqué ?

## Mesures

Tu disposes d'un dispositif expérimental avec 1 noeud Arduino (Adafruit Feather nrf52) connecté à un capteur [VL53L0X](https://www.gotronic.fr/art-capteur-de-distance-120cm-vl53l0x-ada3317-26397.htm) fixé sur une équerre. Le noeud Arduino est connecté sur le PC portable via une liaison USB/série. Sur le PC, tu disposes de programmes Python pour enregistrer les mesures du capteur.
La portée du capteur est de 50 mm à 1200 mm.

```bash
> cd ./savanturiers/code/node_obstacle
> mkdir output/
> ./log_distance.py ./output/ma_mesure00.csv
> ./plot_distance.py ./output/ma_mesure00.csv
```

Peux-tu faire des mesures 'sur table' en intérieur, avec un obstacle placé à :

* 100 mm
* 500 mm
* 1000 mm
* 1500 mm

Tu pourras faire ces séries de mesures avec plusieurs types d'obstacles:

* blanc (taille minimun de face: 200x200 mm)
* noir (taille minimun de face: 200x200 mm)
* blanc avec une hauteur max de  50 mm (pour simuler un trottoir)

Faire des mesures 'en statique' (sans bouger) comme précédamment mais dans un couloir dégagé avec le même type de cibles en face.

Faire des mesures 'en dynamique' (en marchant) dans un couloir vers la cible de 2000mm à 20 mm

Faire des mesures dynamiques et statiques en extérieur pour vérifier l'influence de l'éclairage.
