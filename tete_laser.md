# Tete laser

![tete laser](./tete_laser.jpg)

## Introduction

Le projet consiste à concevoir et réaliser une tête supportant un pointeur avec 2 servo-moteurs. Le pilotage de la tête et l'activation du laser est réalisé grâce à un mini-joystick. Le contrôleur est une carte micro-bit avec la [carte d'extension elecfreaks](https://www.elecfreaks.com/learn-en/microbitKit/Tinker_Kit/index.html)

## Conception mécanique

On utilise [FreeCAD](https://www.freecad.org) pour modéliser les pièces mécaniques.

On peut concevoir la tếte en 3 pièces:
* [la base](./cao/base.FCStd) qui constitue le socle de la tête
* [l'équerre](./cao/bras_final.20241018-164821.FCBak) qui relie les 2 moteurs
* [le support du laser](./cao/support_laser.FCStd) au bout de la tête

A partir de FreeCAD, on peut exporter les pièces au format [STL](https://www.sculpteo.com/fr/centre-apprentissage/creer-un-fichier-3d/quest-ce-quun-fichier-stl/) pour ensuite pouvoir les importer dans PrusaSclicer pour produire le fichier d'instructions gcode pour les imprimantes Prusa de la Halle robotique.

## Montage électronique

On branche sur le microbit:

* le servo-moteur tilt sur la pin P0
* le servo-moteur pan sur la pin P1
* la voie X du joystick sur la pin P2
* la voie Y du joystick sur la pin P3
* le bouton du joystick sur la pin P4
* le pointeur laser sur la pin P5

## Programmation

* Programmation en micro-bit [ici](https://makecode.microbit.org/S78272-36590-73890-58547)