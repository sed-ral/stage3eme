# Imprimante 3D

Tu peux consulter un site [ici](https://devotics.fr/presentation-impression-3d/) pour compléter les informations sur l'impression 3D.

Cherches un objet que tu aimerais imprimer sur le site [thingiverse](https://www.thingiverse.com/) par exemple. Tu peux également choisir de construire ton propre objet en utilisant le site en ligne [thinkercad](https://www.tinkercad.com).
