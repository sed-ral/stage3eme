# Stage3eme

Site pour décrire certains ateliers que tu pourras faire lors de ton stage de de 3ème. Le programme prévu est consultable [ici](https://notes.inria.fr/TbtB_8BlTcmPFXjYSBSR4Q)

Dans un premier temps, tu peux apprendre à mieux connaître l'Inria en allant visiter le site web https://www.inria.fr/
et lire une [présentation](./Inria_ma_prez_2022.pdf) issue de l'[intranet](https://intranet.inria.fr/Vie-pratique/Informer-Communiquer/Identite-l-essentiel/Supports-prets-a-diffuser)

Tu devrais ainsi être capable de répondre à des questions comme:

* Quels sont les objectifs de l'Inria ?
* Peux-tu citer quelles sont ses actions pour l'éducation à la culture numérique ?
* Quels sont les domaines du numériques sur lesquels l'Inria travaille ?
* Qu'est ce qu'une équipe projet ?
* Combien de start-up ont été créee par l'Inria ces 20 dernières années ?
* Peux-tu me décrire un projet de recherche qui tu as remarqué ?
* Qui sont les lauréats des Prix Inria 2021 ?

Dans un deuxième temps, consultes sur https://interstices.info/ et choisit un article (niveau facile) que tu nous présenteras.

## Atelier Marche Artificielle

[Marche Artificielle](./marche_artificielle.md)

## Atelier Shoe-Shoe

[Projet ShoeShoe](./projet_shoeshoe.md)

## Atelier Imprimante 3D

[Imprimante3D](./imprimante3D.md)

## Atelier Réseau de capteurs

[Reseau de capteurs](./reseau_de_capteurs.md)

## Atelier Microbit

[Programmation Microbit](./programmation_microbit.md)

## Projet tête laser

[Tete laser](./tete_laser.md)

