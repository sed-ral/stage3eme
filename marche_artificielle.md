# Marche Artificielle

Après la visite du robot BIP et les explications sur l'analogie entre marche robotique et marche humaine, tu pourras approfondir en visitant les sites:

## Bip en 2000

* [HumansToolbox](http://bipop.inrialpes.fr/software/humans/screenshots.html)
* [Vidéo de Bip](http://www.inrialpes.fr/bipop/bip.html)
* [Vidéo de Bip, version2](https://www.youtube.com/watch?v=iXL29vLakfI)
* [Clip](https://mediatheque.inria.fr/Mediatheque/media/35403)

## Les robot Japonais - 2010

* [Asimo qui tombe](https://www.youtube.com/watch?v=SDRbPN4GgTs)
* [Histoire Asimo](https://youtu.be/xt090WrKU3w)
* [HRP2 qui danse](https://www.youtube.com/watch?v=tnegMm7GiaM)
* [HRP4](https://www.youtube.com/watch?v=c-bgLsvw848)
* [HRP2 et vision](http://www.irisa.fr/lagadic/team/old/Nicolas.Mansard-fra.html)

## Les européens

* [Aldebaran - Nao](https://www.youtube.com/watch?v=rSKRgasUEko)
* [Aldebaran -Pepper](https://www.youtube.com/watch?v=XcJccQqTM6Q)
* [Aldebaran -Romeo](https://www.youtube.com/watch?v=9SpekYGi-9w)
* [PalRobotics - Talos](https://www.youtube.com/watch?v=xUeApfMAKAEL)
* [PalRobotics - Ari](https://www.youtube.com/watch?v=oY9OolA7r4Q)
* [Anybotics](https://www.youtube.com/watch?v=g3odef0EAFA)

## Boston-Dynamics - Le Top !

* [Boston-Dynamics - evolution](https://www.youtube.com/watch?v=iQXErWEPQyQ)
* [Big dog evolution](https://www.youtube.com/watch?v=xqMVg5ixhd0)
* [Spot](https://www.youtube.com/watch?v=A6P6JjVdlE8)
* [DARPA challenge - Atlas V0](https://www.youtube.com/watch?v=zkBnFPBV3f0)
* [DARPA challenge -Schaft](https://www.youtube.com/watch?v=diaZFIUBMBQ)
* [Atlas en 2013](https://www.youtube.com/watch?v=FFGfq0pRczY)
* [Atlas](https://www.youtube.com/watch?v=tF4DML7FIWk)

## Suite de Bip: projet Camin, aide au mouvement humain par électro-stimulation

* [L'homme qui valait 3 milliards](https://interstices.info/lhomme-qui-valait-trois-milliards/)
* [Restaurer la marche grace à une puce - article](https://interstices.info/restaurer-la-marche-grace-a-une-puce/)
* [Restaurer la marche grace à une puce - le film](https://mediatheque.inria.fr/Mediatheque/media/33225)
* [Cybatlhon](https://www.youtube.com/watch?v=Rx9I_hYqQcM)
* [Freewheel](https://www.youtube.com/results?search_query=freewheel+cybathlon)

## Exosquelettes

* [Comment marche un exosquelette](https://www.youtube.com/watch?v=kYETtPGdaeA)
* [wandercraft](https://www.youtube.com/watch?v=V5WC1jeWFPU)
* [wandercraft Bis](https://www.youtube.com/watch?v=-yBfUcFRZ-I)
* [CEA](https://www.youtube.com/watch?v=sKuSZDZPmT4)
* [HAL](https://www.youtube.com/watch?v=yOk61O2rGac)
