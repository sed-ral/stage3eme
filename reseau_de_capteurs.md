# Réseau de capteurs

Après la présentation de Frédéric, quelques vidéos à regarder pour approfondir:

* [Les réseaux de communication](https://www.youtube.com/watch?v=5AVY6E-7yCc)
* [A portée de capteurs](https://www.youtube.com/watch?v=eGc5Vcci3kY)
* [Des capteurs intelligents pour mesurer l'environnement](https://www.youtube.com/watch?v=zsbS310YVe0)
